import React, {useState} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const columnsInitial = [
  {
    name: 'todo',
    color: 'secondary',
    tasks: [
      {
        id: 1,
        name: 'to do 1'
      },
      {
        id: 2,
        name: 'to do 2'
      },
      {
        id: 3,
        name: 'to do 3'
      }
    ]
  },
  {
    name: 'progress',
    color: 'primary',
    tasks: [
      {
        id: 4,
        name: 'to do 4'
      },
      {
        id: 5,
        name: 'to do 5'
      },
      {
        id: 6,
        name: 'to do 6'
      }
    ]
  },
  {
    name: 'review',
    color: 'warning',
    tasks: [
      {
        id: 7,
        name: 'to do 7'
      }
    ]
  },
  {
    name: 'done',
    color: 'success',
    tasks: [
      {
        id: 8,
        name: 'to do 8'
      },
      {
        id: 9,
        name: 'to do 9'
      },
      {
        id: 10,
        name: 'to do 10'
      }
    ]
  }
];
function App() {

  const [columns,setColumns] = useState(columnsInitial)
  const up = (args) =>{
    const reranged = columns.map(col=>{
      if(col.name = args.columnName){
        const tasks  = col.task
         return { ...col, tasks }
      } else{
        return col;
      }
    })
    setColumns(reranged);
  }
  return (
    <div className='container'>
      <h1 className='mb-4 mt-4'>Board</h1>
      <div className ='row'>
        {
          columns.map(col => (
            <div class='col-sm d-flex'>
              <div class={`w-100 border-top border-${col.color} border-width-4`}>
                <h3>{col.name}</h3>
                {
                  col.tasks.map(task => (
                    <div className="card mb-2">
                      <div className="card-body">
                        <h5 className="card-title">
                        {task.name}
                        </h5>
                        <button type="button" className="btn btn-light" onClick={()=> up({
                          columnName: col.name,
                          taskId: task.id 
                        })}>Up</button>
                        <button type="button" className="btn btn-light">Down</button>
                      </div>
                    </div> 

                  ))
                }
              </div>
            </div>
          ))
        }
      </div>

    </div>
  );
}

export default (App);
